/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { Badge, Button } from 'react-bootstrap';
import ReactSpeedometer from 'react-d3-speedometer';

export const Dashboard = () => {
	const [listening, setListening] = useState(false);
	const [cpuUsage, setcpuUsage] = useState(0);
	const [memoryUsage, setmemoryUsage] = useState(0);
	const [count, setCount] = useState(0);

	let eventSource = undefined;

	useEffect(() => {
		if (!listening) {
			const random = Math.round(Math.random() * 10);
			console.log('My User Id: ', random);
			eventSource = new EventSource(
				'http://localhost:8080/event/resources/usage/' + random
			);
			eventSource.onmessage = (event) => {
				const usages = JSON.parse(event.data);
				console.log(usages);
				setCount(usages.length);
				setcpuUsage(usages.cpuUsage);
				setmemoryUsage(usages.memoryUsage);
			};
			eventSource.onerror = (err) => {
				console.error('EventSource failed:', err);
				eventSource.close();
			};
			setListening(true);
		}
		return () => {
			eventSource.close();
			console.log('event closed');
		};
	}, []);

	return (
		<div style={{ marginTop: '20px', textAlign: 'center' }}>
			<Badge bg='secondary'>{count}</Badge>
		</div>
	);
};
