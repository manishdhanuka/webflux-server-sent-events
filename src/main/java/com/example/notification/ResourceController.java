package com.example.notification;

import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
public class ResourceController {

    @CrossOrigin(allowedHeaders = "*")
    @GetMapping(value = "/event/resources/usage/{userId}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<List<Usage>> getResourceUsage(@PathVariable int userId) {

        Random random = new Random();
        List<Usage> usages = new ArrayList<>();

        return Flux.interval(Duration.ofSeconds(1))
                .map(it -> {
                    Usage usage = new Usage(
                            random.nextInt(11),
                            random.nextInt(101),
                            random.nextInt(101),
                            new Date());

                    usages.add(usage);
                    return usages.stream().filter(usageItem -> usageItem.getId() == userId).collect(Collectors.toList());
                });

    }
}
