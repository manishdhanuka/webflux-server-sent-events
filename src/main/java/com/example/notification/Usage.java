package com.example.notification;

import java.util.Date;

public class Usage {
    private int id;
    private int cpuUsage;
    private int memoryUsage;
    private Date date;

    public Usage() {
    }

    public Usage(int id, int cpuUsage, int memoryUsage, Date date) {
        this.id = id;
        this.cpuUsage = cpuUsage;
        this.memoryUsage = memoryUsage;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(int cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(int memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    @Override
    public String toString() {
        return "Usage [id="+id+", cpuUsage="+cpuUsage+", memoryUsage="+memoryUsage+", date="+date+"]";
    }
}
